#include "file_watcher.hpp"

#include <iostream>

namespace game {
	file_watcher::file_watcher(string path) :
		tracked_path(path) {
	}

	map<string, file_state> file_watcher::update() {
		map<string, file_state> ret;
		for (auto & directory_entry : filesystem::directory_iterator(tracked_path)) {
			filesystem::file_time_type mod_time;
			try {
				mod_time = directory_entry.last_write_time();
			} catch (...) {
				cout << "file watch race. skipping and trying again" << endl;
				continue;
			}

			string path = directory_entry.path().string();
			if (tracked_files.find(path) == tracked_files.end()) {
				tracked_files[path] = mod_time;
				ret[path] = file_state::added;
			}
			if ((mod_time - tracked_files[path]) > chrono::milliseconds(20)) {
				tracked_files[path] = mod_time;
				ret[path] = file_state::modified;
			}
		}

		auto tracked_files_copy(tracked_files);
		for (auto & tf : tracked_files_copy) {
			if (!filesystem::is_regular_file(tf.first)) {
				ret[tf.first] = file_state::removed;
				tracked_files.erase(tf.first);
			}
		}
		return ret;
	}
}
