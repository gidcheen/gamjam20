#pragma once

#include "object.hpp"

#include <renderer.hpp>
#include <camera.hpp>
#include <sprite.hpp>
#include <glm/glm.hpp>

namespace game {
	using namespace glm;
	using namespace graphics;

	class prop : public object {
	private:
		shared_ptr<renderer> object_renderer;
		shared_ptr<renderable> object_renderable;

	public:
		prop(shared_ptr<renderer> object_renderer, shared_ptr<camera> cam, shared_ptr<sprite> object_sprite, mat3 transform);
		~prop() override;
	};
}
