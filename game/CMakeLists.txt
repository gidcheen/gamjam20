project(Game)

add_executable(game
        main.cpp

        world.cpp
        object.cpp
        object_factory.cpp

        player.cpp
        interactable.cpp
        pickupable.cpp
        bounding_box.cpp
        door.cpp
        key.cpp
        pot.cpp
        prop.cpp
        background.cpp

        special_key.cpp
        special_key_part.cpp
        chest.cpp
        smelter.cpp

        quest.cpp

        file_watcher.cpp

        )

target_link_libraries(game glad glfw glm stb graphics ui)

if (WIN32)
else ()
    target_link_libraries(game stdc++fs)
endif ()