#pragma once

#include "pickupable.hpp"

namespace game {
	class special_key : public pickupable {
	public:
		special_key(
			shared_ptr<renderer> door_renderer,
			shared_ptr<camera> cam,
			mat3 transform
		);

		string interaction_text(world & main_world) const override;
		bool is_interactable(world & main_world) const override;
	};
}
