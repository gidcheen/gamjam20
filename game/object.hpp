#pragma once

namespace game {
	class world;

	class object {
	public:
		virtual ~object() = default;
		virtual void update(world & main_world, float delta_time, float aspect);
		virtual bool needs_update();
	};
}
