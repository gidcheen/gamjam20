#include "special_key_part.hpp"

#include <memory>
#include <shader.hpp>
#include <material.hpp>
#include <mesh.hpp>

namespace game {

	special_key_part::special_key_part(
		shared_ptr<renderer> door_renderer,
		shared_ptr<camera> cam,
		mat3 transform,
		special_key_part::part_type type) :
		pickupable(
			move(door_renderer),
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				make_shared<sprite>(image::load("images/special_key_part_" + get_part_name(type) + ".png").value()),
				0,
				transform
			)
		),
		type(type) {
	}

	string special_key_part::interaction_text(world & main_world) const {
		return "pick up special key piece " + get_part_name(type);
	}

	special_key_part::part_type special_key_part::get_part_type() const {
		return type;
	}

	bool special_key_part::is_interactable(world & main_world) const {
		return is_pickupable;
	}

	string special_key_part::get_part_name(special_key_part::part_type type) {
		if (type == special_key_part::part_type::left) {
			return "left";
		} else if (type == special_key_part::part_type::mid) {
			return "mid";
		} else if (type == special_key_part::part_type::right) {
			return "right";
		}
		return "";
	}
}
