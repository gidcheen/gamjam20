#include "chest.hpp"

#include <algorithm>

#include <shader.hpp>
#include <mesh.hpp>
#include <material.hpp>

#include <glm/gtx/matrix_transform_2d.hpp>

#include "world.hpp"
#include "special_key_part.hpp"
#include "key.hpp"
#include "player.hpp"

namespace game {
	chest::chest(
		shared_ptr<renderer> door_renderer,
		shared_ptr<camera> cam,
		mat3 transform,
		special_key_part::part_type to_drop
	) :
		interactable(
			move(door_renderer),
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				make_shared<sprite>(image::load("images/chest.png").value()),
				transform[2][1] / 128,
				transform
			)
		),
		to_drop(to_drop) {
	}

	void chest::interact(world & main_world) {
		for (auto & o : main_world.objects) {
			auto p = dynamic_pointer_cast<player>(o);
			if (p) {
				auto player_key = dynamic_pointer_cast<key>(p->get_current_pickupable());
				if (player_key) {
					interactable::interact(main_world);
					main_world.objects.push_back(
						make_shared<special_key_part>(
							sprite_renderer,
							sprite_renderable->camera_ptr,
							translate(sprite_renderable->transform, vec2(-18, -4)),
							to_drop
						)
					);
					is_open = true;
					sprite_renderable->sprite_ptr = make_shared<sprite>(image::load("images/chest_open.png").value());
					auto it = find(main_world.objects.begin(), main_world.objects.end(), player_key);
					main_world.objects.erase(it);
				}
			}
		}
	}

	string chest::interaction_text(world & main_world) const {
		for (auto & o : main_world.objects) {
			auto p = dynamic_pointer_cast<player>(o);
			if (p) {
				auto player_key = dynamic_pointer_cast<key>(p->get_current_pickupable());
				if (player_key) {
					return "search this chest";
				}
			}
		}
		return "needs key";
	}

	bool chest::is_interactable(world & main_world) const {
		return !is_open;
	}
}
