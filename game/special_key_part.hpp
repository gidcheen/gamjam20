#pragma once

#include <renderer.hpp>
#include <camera.hpp>
#include <glm/glm.hpp>

#include "pickupable.hpp"

namespace game {
	class special_key_part : public pickupable {
	public:
		enum class part_type {
			left, mid, right
		};

		static string get_part_name(part_type type);

	private:
		part_type type;

	public:
		bool is_pickupable{true};

	public:
		special_key_part(
			shared_ptr<renderer> door_renderer,
			shared_ptr<camera> cam,
			mat3 transform,
			part_type type
		);

		string interaction_text(world & main_world) const override;
		bool is_interactable(world & main_world) const override;

		part_type get_part_type() const;
	};
}
