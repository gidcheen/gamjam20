#include "pot.hpp"

#include <renderer.hpp>
#include <renderable.hpp>
#include <camera.hpp>
#include <shader.hpp>
#include <mesh.hpp>
#include <material.hpp>
#include <sprite.hpp>
#include <image.hpp>

#include "world.hpp"
#include "key.hpp"

namespace game {

	pot::pot(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform) :
		interactable(
			move(door_renderer),
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				make_shared<sprite>(image::load("images/plant_left.png").value()),
				transform[2][1] / 128,
				transform
			)
		) {
	}

	void pot::interact(world & main_world) {
		interactable::interact(main_world);

		interaction_count++;
		if ((interaction_count % 3) == 0) {
			main_world.objects.push_back(
				make_shared<key>(
					sprite_renderer,
					sprite_renderable->camera_ptr, sprite_renderable->transform
				)
			);
		}

		int self_index = 0;
		for (auto & o : main_world.objects) {
			if (o.get() == this) {
				break;
			}
			self_index++;
		}
		main_world.objects.erase(main_world.objects.begin() + self_index);
	}

	string pot::interaction_text(world & main_world) const {
		return "Search pot for key";
	}

	int pot::get_interaction_count() {
		return interaction_count;
	}

	bool pot::is_interactable(world & main_world) const {
		return true;
	}
}
