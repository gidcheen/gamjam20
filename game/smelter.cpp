#include "smelter.hpp"

#include <renderer.hpp>
#include <renderable.hpp>
#include <camera.hpp>
#include <shader.hpp>
#include <mesh.hpp>
#include <material.hpp>

#include <glm/gtx/matrix_transform_2d.hpp>

#include "world.hpp"
#include "player.hpp"
#include "special_key.hpp"

namespace game {
	smelter::smelter(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform) :
		interactable(
			move(door_renderer),
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				make_shared<sprite>(image::load("images/smelter.png").value()),
				transform[2][1] / 128.0,
				transform
			)
		) {
	}

	void smelter::interact(world & main_world) {
		interactable::interact(main_world);

		for (auto & o : main_world.objects) {
			auto p = dynamic_pointer_cast<player>(o);
			if (p) {
				auto player_key_part = dynamic_pointer_cast<special_key_part>(p->get_current_pickupable());
				if (player_key_part) {
					keys[player_key_part->get_part_type()] = player_key_part;
					p->pick_up(nullptr);
					player_key_part->is_pickupable = false;
					player_key_part->get_renderable()->transform =
						translate(sprite_renderable->transform, vec2(10 + int(player_key_part->get_part_type()) * 2, 25));
				}
				if (
					keys[special_key_part::part_type::left].lock() != nullptr &&
						keys[special_key_part::part_type::mid].lock() != nullptr &&
						keys[special_key_part::part_type::right].lock() != nullptr
					) {
					main_world.objects.push_back(
						make_shared<special_key>(
							sprite_renderer,
							sprite_renderable->camera_ptr,
							sprite_renderable->transform
						)
					);
				}
			}
		}
	}

	string smelter::interaction_text(world & main_world) const {
		for (auto & o : main_world.objects) {
			auto p = dynamic_pointer_cast<player>(o);
			if (p) {
				auto player_key_part = dynamic_pointer_cast<special_key_part>(p->get_current_pickupable());
				if (player_key_part) {
					return "add key part";
				}
			}
		}
		return "";
	}

	bool smelter::is_interactable(world & main_world) const {
		for (auto & o : main_world.objects) {
			auto p = dynamic_pointer_cast<player>(o);
			if (p) {
				auto player_key_part = dynamic_pointer_cast<special_key_part>(p->get_current_pickupable());
				if (player_key_part) {
					return true;
				}
			}
		}
		return false;
	}
}
