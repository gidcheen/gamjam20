#include "special_key.hpp"

#include <shader.hpp>
#include <material.hpp>
#include <mesh.hpp>

namespace game {
	special_key::special_key(
		shared_ptr<renderer> door_renderer,
		shared_ptr<camera> cam,
		mat3 transform
	) : pickupable(
		move(door_renderer),
		make_shared<renderable>(
			make_shared<shader>(nullopt, nullopt),
			make_shared<material>(),
			make_shared<mesh>(),
			move(cam),
			make_shared<sprite>(image::load("images/special_key.png").value()),
			0,
			transform
		)
		) {
	}

	string special_key::interaction_text(world & main_world) const {
		return string();
	}

	bool special_key::is_interactable(world & main_world) const {
		return true;
	}
}
