#include "world.hpp"

#include <memory>
#include <algorithm>

#include <glm/gtx/matrix_transform_2d.hpp>

#include "object_factory.hpp"
#include "object.hpp"

namespace game {
	game::world::world(GLFWwindow * window) {
		main_renderer = make_shared<renderer>();
		main_camera = make_shared<camera>();
		main_camera->width = 256;
		main_camera->transform = translate(mat3(1), vec2(0, 50));

		auto main_font = make_shared<font>(
			make_shared<sprite>(image::load("fonts/pixel_font.png").value())
		);

		factory = make_shared<object_factory>(
			main_renderer,
			main_camera,
			main_font,
			window
		);
	}

	void world::update(float delta_time, float aspect) {
		auto all_to_reload = watcher.update();
		for (auto & to_reload : all_to_reload) {
			if (to_reload.second == file_state::removed || to_reload.second == file_state::modified) {
				auto it = find(objects.begin(), objects.end(), object_paths[to_reload.first].lock());
				objects.erase(it);
			}
			if (to_reload.second == file_state::added || to_reload.second == file_state::modified) {
				auto obj = factory->create(to_reload.first);
				if (obj) {
					objects.push_back(obj);
					object_paths[to_reload.first] = obj;
				}
			}
		}

		auto objects_copy(objects);
		for (auto & o : objects_copy) {
			if (o->needs_update()) {
				o->update(*this, delta_time, aspect);
			}
		}
		main_renderer->render(aspect);
	}
}