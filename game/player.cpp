#include "player.hpp"

#include <algorithm>

#include <renderer.hpp>
#include <renderable.hpp>
#include <camera.hpp>
#include <shader.hpp>
#include <mesh.hpp>
#include <material.hpp>
#include <sprite.hpp>
#include <image.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>

#include "world.hpp"
#include "interactable.hpp"
#include "pickupable.hpp"

namespace game {
	float player::get_horizontal_input() const {
		if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
			return -1;
		} else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
			return 1;
		}
		return 0;
	}

	float player::get_vertical_input() const {
		if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
			return 1;
		} else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
			return -1;
		}
		return 0;
	}

	void player::handle_interactions(world & w) {
		shared_ptr<interactable> current_interactable;
		for (auto & o : w.objects) {
			if (o == current_pickupable.lock()) {
				continue;
			}
			auto i = dynamic_pointer_cast<interactable>(o);
			if (!i) {
				continue;
			}
			if (!i->is_interactable(w)) {
				continue;
			}
			if (!get_bounding_box().is_overlapping(i->get_bounding_box())) {
				continue;
			}
			current_interactable = i;

			break;
		}

		auto current_interact_key_state = glfwGetKey(window, GLFW_KEY_SPACE);
		if (current_interactable) {
			interact_text->set_text(current_interactable->interaction_text(w));
			if (current_interact_key_state == GLFW_RELEASE && last_interact_key_state == GLFW_PRESS) {
				current_interactable->interact(w);
			}
		} else {
			interact_text->set_text("");
		}
		last_interact_key_state = current_interact_key_state;
	}

	player::player(GLFWwindow * window, shared_ptr<renderer> player_renderer, shared_ptr<camera> cam, shared_ptr<font> pixel_font, mat3 transform) :
		player_renderer(move(player_renderer)),
		window(window),
		sprite_left(make_shared<sprite>(image::load("images/player_left.png").value())),
		sprite_right(make_shared<sprite>(image::load("images/player_right.png").value())),
		player_renderable(
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				cam,
				sprite_left,
				0.1f,
				transform
			)
		),
		interact_text(
			make_shared<text>(
				this->player_renderer, cam, move(pixel_font), "", 0, mat3(1)
			)
		),
		bb(
			glm::vec2(),
			glm::vec2(
				this->player_renderable->sprite_ptr->get_image().get_width(),
				this->player_renderable->sprite_ptr->get_image().get_height()
			)
		) {
		this->player_renderer->submit(this->player_renderable);
	}

	player::~player() {
		player_renderer->remove(player_renderable);
	}

	void player::update(world & main_world, float delta_time, float aspect) {
		auto horizontal = get_horizontal_input();
		auto vertical = get_vertical_input();

		// update position
		player_renderable->transform = glm::translate(
			player_renderable->transform,
			glm::vec2(horizontal * speed * delta_time, vertical * speed * 0.7 * delta_time)
		);

		auto width = player_renderable->sprite_ptr->get_image().get_width();
		auto position = glm::vec2(player_renderable->transform[2]);
		player_renderable->transform[2][0] = std::clamp(position.x, min_pos.x, max_pos.x - width);
		player_renderable->transform[2][1] = std::clamp(position.y, min_pos.y, max_pos.y);

		player_renderable->depth = position.y / 128;

		// update sprite
		if (horizontal != 0) {
			player_renderable->sprite_ptr = horizontal > 0 ? sprite_right : sprite_left;
		}

		handle_interactions(main_world);

		if (current_pickupable.lock()) {
			auto player_half_width = player_renderable->sprite_ptr->get_image().get_width() / 2.0;
			auto key_half_width = current_pickupable.lock()->get_renderable()->sprite_ptr->get_image().get_width() / 2.0;
			current_pickupable.lock()->get_renderable()->transform = translate(
				player_renderable->transform,
				vec2((player_renderable->sprite_ptr == sprite_right ? 20 : -20) - key_half_width + player_half_width, 30)
			);
		}

		interact_text->set_transform(translate(player_renderable->transform, vec2(0, 64)));
	}

	bool player::needs_update() {
		return true;
	}

	bounding_box player::get_bounding_box() const {
		return bb * player_renderable->transform;
	}

	void player::pick_up(shared_ptr<pickupable> to_pickup) {
		current_pickupable = to_pickup;
	}

	shared_ptr<pickupable> player::get_current_pickupable() const {
		return current_pickupable.lock();
	}
}