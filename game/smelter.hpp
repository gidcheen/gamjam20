#pragma once

#include <map>

#include <renderer.hpp>

#include "interactable.hpp"
#include "special_key_part.hpp"

namespace game {
	using namespace std;

	class smelter : public interactable {
	public:
		map<special_key_part::part_type, weak_ptr<special_key_part>> keys;

		smelter(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform);

		void interact(world & main_world) override;
		string interaction_text(world & main_world) const override;
		bool is_interactable(world & main_world) const override;
	};
}
