#pragma once

#include "interactable.hpp"

namespace game {
	
	class bounding_box;

	class pickupable : public interactable {
	public:
		pickupable(shared_ptr<renderer> sprite_renderer, shared_ptr<renderable> sprite_renderable);
		~pickupable() override = default;

		void interact(world & main_world) override;

		virtual void put_down(world & main_world);
	};
}
