#pragma once

#include "interactable.hpp"

#include "special_key_part.hpp"

namespace game {
	class chest : public interactable {
	private:
		bool is_open{false};
		special_key_part::part_type to_drop;

	public:
		chest(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform, special_key_part::part_type to_drop);

		void interact(world & main_world) override;
		string interaction_text(world & main_world) const override;
		bool is_interactable(world & main_world) const override;
	};
}
