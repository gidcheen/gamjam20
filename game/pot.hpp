#pragma once

#include "interactable.hpp"

namespace game {
	class pot : public interactable {
		inline static int interaction_count{0};

	public:
		pot(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform);

		string interaction_text(world & main_world) const override;
		void interact(world & main_world) override;
		bool is_interactable(world & main_world) const override;

		static int get_interaction_count();
	};
}
