#include "bounding_box.hpp"

namespace game {
	bounding_box::bounding_box(const glm::vec2 & top_left, const glm::vec2 & bottom_right) :
		bottom_left(top_left),
		top_right(bottom_right) {
	}

	bounding_box::bounding_box(const bounding_box & other) :
		bottom_left(other.bottom_left),
		top_right(other.top_right) {
	}

	bounding_box::~bounding_box() {
	}

	bool bounding_box::is_overlapping(const bounding_box & other) const {
		return bottom_left.x < other.top_right.x &&
			top_right.x > other.bottom_left.x &&
			bottom_left.y < other.top_right.y &&
			top_right.y > other.bottom_left.y;
	}

	bounding_box bounding_box::operator*(const glm::mat3 & transform) const {
		return bounding_box(transform * glm::vec3(bottom_left, 1), transform * glm::vec3(top_right, 1));
	}

	std::ostream & operator<<(std::ostream & os, const bounding_box & bb) {
		os << "{top_left: " << glm::to_string(bb.bottom_left) << ", bottom_right: " << glm::to_string(bb.top_right) << "}";
		return os;
	}
}
