#include "prop.hpp"


#include <renderable.hpp>
#include <shader.hpp>
#include <material.hpp>
#include <mesh.hpp>

namespace game {
	prop::prop(shared_ptr<renderer> object_renderer, shared_ptr<camera> cam, shared_ptr<sprite> object_sprite, mat3 transform) :
		object_renderer(move(object_renderer)),
		object_renderable(
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				move(object_sprite),
				transform[2][1] / 128,
				transform
			)
		) {
		this->object_renderer->submit(this->object_renderable);
	}

	prop::~prop() {
		this->object_renderer->remove(this->object_renderable);
	}
}
