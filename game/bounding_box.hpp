#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>

namespace game {
	using namespace glm;
	using namespace std;

	class bounding_box {
	private:
		glm::vec2 bottom_left;
		glm::vec2 top_right;

	public:
		bounding_box(const glm::vec2& top_left, const glm::vec2& bottom_right);
		bounding_box(const bounding_box& other);
		~bounding_box();

		[[nodiscard]] bool is_overlapping(const bounding_box& other) const;

		bounding_box operator*(const glm::mat3& transform) const;

		friend std::ostream& operator<<(std::ostream& os, const bounding_box& bb);
	};
}
