#pragma once

#include "object.hpp"

#include <renderer.hpp>
#include <camera.hpp>
#include <sprite.hpp>
#include <glm/glm.hpp>

namespace game {
	using namespace glm;
	using namespace graphics;

	class background : public object {
	private:
		shared_ptr<renderer> object_renderer;
		shared_ptr<renderable> object_renderable;

	public:
		background(shared_ptr<renderer> object_renderer, shared_ptr<camera> cam, shared_ptr<sprite> object_sprite, float  depth_offset, mat3 transform);
		~background() override;
	};
}
