#include "key.hpp"

#include <renderer.hpp>
#include <renderable.hpp>
#include <shader.hpp>
#include <mesh.hpp>
#include <material.hpp>
#include <image.hpp>

#include "world.hpp"

namespace game {
	key::key(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform) :
		pickupable(
			move(door_renderer),
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				make_shared<sprite>(image::load("images/key.png").value()),
				0,
				transform
			)
		) {
	}

	string key::interaction_text(world & main_world) const {
		return "pick up key";
	}

	bool key::is_interactable(world & main_world) const {
		return true;
	}
}
