#include "quest.hpp"

#include <algorithm>
#include <sstream>

#include <glm/gtx/matrix_transform_2d.hpp>

#include "world.hpp"
#include <renderer.hpp>
#include <renderable.hpp>
#include <camera.hpp>
#include <shader.hpp>
#include <mesh.hpp>
#include <material.hpp>
#include <sprite.hpp>
#include <image.hpp>
#include <text.hpp>

#include "door.hpp"
#include "pot.hpp"
#include "key.hpp"

namespace game {
	using namespace glm;

	quest::quest(
		shared_ptr<renderer> main_renderer,
		shared_ptr<camera> cam,
		shared_ptr<font> pixel_font,
		int nr,
		string title
	) :
		main_renderer(move(main_renderer)),
		cam(move(cam)),
		bg_sprite(
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				this->cam,
				make_shared<sprite>(image::load("images/box.png").value()),
				float(nr) / 10.0f + 0.01f,
				mat3(1)
			)
		),
		nr(nr) {

		std::stringstream ss(title);
		std::string line;
		while (getline(ss, line, '\\')) {
			lines.push_back(
				make_shared<text>(
					this->main_renderer,
					this->cam,
					pixel_font,
					move(line),
					float(nr) / 10.0f,
					mat3(1)
				)
			);
		}
		this->main_renderer->submit(bg_sprite);
	}

	quest::~quest() {
		this->main_renderer->remove(bg_sprite);
	}

	void quest::update(world & main_world, float delta_time, float aspect) {
		mat3 trans(1);
		bg_sprite->transform = translate(trans, vec2(-cam->width, cam->width * aspect));
		auto height = bg_sprite->sprite_ptr->get_image().get_height();

		const int char_height = 8;
		const int offset = 4;
		int line_index{0};
		for (auto & title_line : lines) { //height - 12 - 12 * line_index
			title_line->set_transform(translate(bg_sprite->transform, vec2(offset, height - char_height * (line_index + 1) - offset)));
			line_index++;
		}

		if (is_done(main_world)) {
			auto it = find_if(main_world.objects.begin(), main_world.objects.end(), [&](shared_ptr<object> & obj) {
				return obj.get() == this;
			});
			if (it != main_world.objects.begin()) {
				main_world.objects.erase(it);
			}
		}
	}

	bool quest::needs_update() {
		return true;
	}
	bool quest::is_done(world & main_world) {
		switch (nr) {
			case 0:
				return door::get_tried_to_open();
			case 1:
				return pot::get_interaction_count() >= 1;
			case 2:
				return pot::get_interaction_count() >= 2;
			case 3:
				return pot::get_interaction_count() >= 3;
			case 4:
				for (auto & o : main_world.objects) {
					auto main_door = dynamic_pointer_cast<door>(o);
					if (main_door) {
						return main_door->get_tried_to_unlock();
					}
				}
				return false;
		}
		return false;
	}
}