#pragma once

#include "interactable.hpp"

namespace game {
	class door : public interactable {
	private:
		bool is_open{false};
		inline static bool tried_to_open{false};
		inline static bool tried_to_unlock{false};

	public:
		door(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform);

		void interact(world & main_world) override;
		string interaction_text(world & main_world) const override;
		bool is_interactable(world & main_world) const override;

		static bool get_tried_to_open();
		static bool get_tried_to_unlock();
	};
}
