#pragma once

#include <sprite.hpp>
#include <text.hpp>

#include "object.hpp"

namespace game {
	using namespace std;
	using namespace graphics;
	using namespace ui;

	class quest : public object {
	public:
		shared_ptr<renderer> main_renderer;
		shared_ptr<camera> cam;
		shared_ptr<renderable> bg_sprite;

		vector<shared_ptr<text>> lines;

		int nr;

	public:
		quest(
			shared_ptr<renderer> main_renderer,
			shared_ptr<camera> cam,
			shared_ptr<font> pixel_font,
			int nr,
			string title
		);
		~quest() override;

		void update(world & main_world, float delta_time, float aspect) override;
		bool needs_update() override;

	private:
		bool is_done(world & main_world);
	};
}
