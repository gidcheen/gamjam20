#include "interactable.hpp"

#include <iostream>

namespace game {
	interactable::interactable(shared_ptr<renderer> sprite_renderer, shared_ptr<renderable> sprite_renderable) :
		sprite_renderer(move(sprite_renderer)),
		sprite_renderable(move(sprite_renderable)),
		bb(
			vec2(),
			vec2(
				this->sprite_renderable->sprite_ptr->get_image().get_width(),
				this->sprite_renderable->sprite_ptr->get_image().get_height()
			)
		) {
		this->sprite_renderer->submit(this->sprite_renderable);
	}

	interactable::~interactable() {
		sprite_renderer->remove(this->sprite_renderable);
	}

	shared_ptr<renderable> interactable::get_renderable() {
		return sprite_renderable;
	}

	bounding_box interactable::get_bounding_box() const {
		return bb * sprite_renderable->transform;
	}

	void interactable::interact(world & main_world) {
	}
}
