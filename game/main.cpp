#include <iostream>
#include <filesystem>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <debug.hpp>

#include "world.hpp"

using namespace std;
using namespace game;

int main(int argc, char ** argv) {
	if (argc >= 2) {
		filesystem::current_path(argv[1]);
	}

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	auto window = glfwCreateWindow(1500, 1000, "GameJam20", nullptr, nullptr);
	if (window == nullptr) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
	glEnable(GL_DEPTH_TEST);
	glfwSetFramebufferSizeCallback(window, [](GLFWwindow * w, int width, int height) {
		glViewport(0, 0, width, height);
	});

	graphics::setup_debug();

	try {
		world main_world(window);

		auto time_last_frame = glfwGetTime();
		while (!glfwWindowShouldClose(window)) {
			auto time_now = glfwGetTime();
			auto delta_time = time_now - time_last_frame;
			time_last_frame = time_now;

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			array<GLint, 4> viewport_size{};
			glGetIntegerv(GL_VIEWPORT, viewport_size.data());
			float aspect = (float) viewport_size[3] / (float) viewport_size[2];

			main_world.update(delta_time, aspect);

			glfwSwapBuffers(window);
			glfwPollEvents();

			if (main_world.did_win) {
				break;
			}
		}
	}
	catch (int e) {
	}

	glfwTerminate();

	return 0;
}