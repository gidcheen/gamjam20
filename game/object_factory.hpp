#pragma once

#include <memory>
#include <renderer.hpp>
#include <camera.hpp>
#include <font.hpp>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

namespace game {
	using namespace std;
	using namespace game;
	using namespace ui;

	class object;

	class object_factory {
	private:
		shared_ptr<renderer> main_renderer;
		shared_ptr<camera> main_camera;
		shared_ptr<font> main_font;
		GLFWwindow * window;

	public:
		object_factory(
			shared_ptr<renderer> main_renderer,
			shared_ptr<camera> main_camera,
			shared_ptr<font> main_font,
			GLFWwindow * window
		);
		shared_ptr<object> create(string path);
	};
}
