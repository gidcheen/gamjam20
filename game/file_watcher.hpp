#pragma once

#include <map>
#include <filesystem>

namespace game {
	using namespace std;

	enum class file_state {
		added, modified, removed
	};

	class file_watcher {
	public:

	private:
		filesystem::path tracked_path;
		map<string, filesystem::file_time_type> tracked_files;

	public:
		file_watcher(string path);

		map<string, file_state> update();
	};
}
