#include "object_factory.hpp"

#include <fstream>
#include <map>
#include <sstream>

#include <glm/glm.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>

#include <font.hpp>

#include <renderer.hpp>
#include <camera.hpp>

#include "player.hpp"
#include "door.hpp"
#include "pot.hpp"
#include "key.hpp"
#include "prop.hpp"
#include "background.hpp"
#include "quest.hpp"
#include "chest.hpp"
#include "smelter.hpp"
#include "special_key.hpp"
#include "special_key_part.hpp"

namespace game {
	using namespace glm;

	object_factory::object_factory(
		shared_ptr<renderer> main_renderer,
		shared_ptr<camera> main_camera,
		shared_ptr<font> main_font,
		GLFWwindow * window
	) :
		main_renderer(move(main_renderer)),
		main_camera(move(main_camera)),
		main_font(move(main_font)),
		window(window) {

	}

	shared_ptr<object> object_factory::create(string path) {
		ifstream file_content(path);
		string type;
		file_content >> type;

		map<string, string> properties;
		string line;
		while (getline(file_content, line, '\n')) {
			stringstream ss(line);
			string key, value;
			ss >> key;
			string temp;
			while (ss >> temp) {
				value += temp + " ";
			}
			if (!value.empty()) {
				value.erase(value.end() - 1);
			}
			properties[key] = value;
		}

		auto get_or = [&](string const & key, string const & default_value) {
			auto x = properties.find(key);
			return x != properties.end() ? x->second : default_value;
		};

		if (type == "player") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			return make_shared<player>(window, main_renderer, main_camera, main_font, translate(mat3(1), position));
		} else if (type == "door") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			return make_shared<door>(main_renderer, main_camera, translate(mat3(1), position));
		} else if (type == "pot") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			return make_shared<pot>(main_renderer, main_camera, translate(mat3(1), position));
		} else if (type == "key") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			return make_shared<key>(main_renderer, main_camera, translate(mat3(1), position));
		} else if (type == "prop") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			string sprite_path = get_or("path:", "");
			auto prop_image = image::load(sprite_path);
			if (prop_image) {
				auto background_sprite = make_shared<sprite>(prop_image.value());
				return make_shared<prop>(main_renderer, main_camera, background_sprite, translate(mat3(1), position));
			}
		} else if (type == "background") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			float offset = stof(get_or("offset:", "0"));
			string sprite_path = get_or("path:", "");
			auto background_image = image::load(sprite_path);
			if (background_image) {
				auto background_sprite = make_shared<sprite>(background_image.value());
				return make_shared<background>(main_renderer, main_camera, background_sprite, offset, translate(mat3(1), position));
			}
		} else if (type == "quest") {
			int nr = stoi(get_or("nr:", "0"));
			string title = get_or("title:", "No title");
			return make_shared<quest>(main_renderer, main_camera, main_font, nr, title);
		} else if (type == "chest") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			string key_type_string = get_or("part:", "left");
			special_key_part::part_type part_type{special_key_part::part_type::left};
			if (key_type_string == "left") {
				part_type = special_key_part::part_type::left;
			} else if (key_type_string == "mid") {
				part_type = special_key_part::part_type::mid;
			} else if (key_type_string == "right") {
				part_type = special_key_part::part_type::right;
			}
			return make_shared<chest>(main_renderer, main_camera, translate(mat3(1), position), part_type);
		} else if (type == "smelter") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			return make_shared<smelter>(main_renderer, main_camera, translate(mat3(1), position));
		} else if (type == "special_key") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			return make_shared<special_key>(main_renderer, main_camera, translate(mat3(1), position));
		} else if (type == "special_key_part") {
			vec2 position{stof(get_or("x:", "0")), stof(get_or("y:", "0"))};
			string key_type_string = get_or("part:", "left");
			special_key_part::part_type part_type{special_key_part::part_type::left};
			if (key_type_string == "left") {
				part_type = special_key_part::part_type::left;
			} else if (key_type_string == "mid") {
				part_type = special_key_part::part_type::mid;
			} else if (key_type_string == "right") {
				part_type = special_key_part::part_type::right;
			}
			return make_shared<special_key_part>(main_renderer, main_camera, translate(mat3(1), position), part_type);
		}

		return nullptr;
	}
}
