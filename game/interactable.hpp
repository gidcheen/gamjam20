#pragma once

#include <memory>
#include <string>

#include <renderer.hpp>
#include <renderable.hpp>
#include <sprite.hpp>
#include "bounding_box.hpp"

#include "object.hpp"

namespace game {
	using namespace std;
	using namespace graphics;

	class world;
	class player;

	class interactable : public object {
	protected:
		shared_ptr<renderer> sprite_renderer;
		shared_ptr<renderable> sprite_renderable;
		bounding_box bb;

	public:
		interactable(shared_ptr<renderer> sprite_renderer, shared_ptr<renderable> sprite_renderable);
		~interactable() override;

		virtual void interact(world & main_world);
		virtual bool is_interactable(world & main_world) const = 0;
		virtual string interaction_text(world & main_world) const = 0;

		shared_ptr<renderable> get_renderable();
		[[nodiscard]] bounding_box get_bounding_box() const noexcept;
	};
}
