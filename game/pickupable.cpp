#include "pickupable.hpp"

#include <utility>
#include <iostream>

#include "world.hpp"
#include "player.hpp"

namespace game {
	using namespace std;

	pickupable::pickupable(shared_ptr<renderer> sprite_renderer, shared_ptr<renderable> sprite_renderable) :
		interactable(move(sprite_renderer), move(sprite_renderable)) {

	}

	void pickupable::interact(world & main_world) {
		interactable::interact(main_world);
		shared_ptr<object> self;
		for (auto & o : main_world.objects) {
			if (o.get() == this) {
				self = o;
				break;
			}
		}
		for (auto & o : main_world.objects) {
			auto main_player = dynamic_pointer_cast<player>(o);
			if (main_player) {
				main_player->pick_up(dynamic_pointer_cast<pickupable>(self));
			}
		}
	}

	void pickupable::put_down(world & main_world) {
	}
}
