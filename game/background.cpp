#include "background.hpp"

#include <renderable.hpp>
#include <shader.hpp>
#include <material.hpp>
#include <mesh.hpp>

namespace game {
	background::background(
	        shared_ptr<renderer> object_renderer,
	        shared_ptr<camera> cam,
	        shared_ptr<sprite> object_sprite,
	        float depth_offset, mat3 transform) :
		object_renderer(move(object_renderer)),
		object_renderable(
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				move(object_sprite),
				0.99f - depth_offset,
				transform
			)
		) {
		object_renderer->submit(this->object_renderable);
	}

	background::~background() {
		object_renderer->remove(this->object_renderable);
	}
}
