#include "door.hpp"

#include <renderer.hpp>
#include <renderable.hpp>
#include <camera.hpp>
#include <shader.hpp>
#include <mesh.hpp>
#include <material.hpp>
#include <sprite.hpp>
#include <image.hpp>

#include "player.hpp"
#include "world.hpp"
#include "key.hpp"
#include "special_key.hpp"

namespace game {
	door::door(shared_ptr<renderer> door_renderer, shared_ptr<camera> cam, mat3 transform) :
		interactable(
			move(door_renderer),
			make_shared<renderable>(
				make_shared<shader>(nullopt, nullopt),
				make_shared<material>(),
				make_shared<mesh>(),
				move(cam),
				make_shared<sprite>(image::load("images/door.png").value()),
				transform[2][1] / 128,
				transform
			)
		) {
	}

	void door::interact(world & main_world) {
		interactable::interact(main_world);
		tried_to_open = true;

		for (auto & o : main_world.objects) {
			auto p = dynamic_pointer_cast<player>(o);
			if (p) {
				auto player_key = dynamic_pointer_cast<key>(p->get_current_pickupable());
				if(player_key) {
					tried_to_unlock = true;
					break;
				}

				auto player_special_key = dynamic_pointer_cast<special_key>(p->get_current_pickupable());
				if (player_special_key) {
					is_open = true;
					main_world.did_win = true;
					auto open_image = image::load("images/door_open.png");
					sprite_renderable->sprite_ptr = make_shared<sprite>(open_image.value());
					break;
				}
			}
		}
	}

	string door::interaction_text(world & main_world) const {
		return "Try to open door";
	}

	bool door::get_tried_to_open() {
		return tried_to_open;
	}

	bool door::get_tried_to_unlock() {
		return tried_to_unlock;
	}

	bool door::is_interactable(world & main_world) const {
		return true;
	}
}
