#pragma once

#include <memory>
#include <vector>
#include <filesystem>
#include <map>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <renderer.hpp>
#include <camera.hpp>
#include <font.hpp>

#include "file_watcher.hpp"

namespace game {
	using namespace std;
	using namespace graphics;
	using namespace ui;

	class object;
	class object_factory;

	class world {
	public:

		shared_ptr<renderer> main_renderer;
		shared_ptr<camera> main_camera;

		vector<shared_ptr<object>> objects;

		bool did_win{false};

	private:
		file_watcher watcher{"objects"};
		shared_ptr<object_factory> factory;

		map<string, weak_ptr<object>> object_paths;

	public:
		explicit world(GLFWwindow * window);

		void update(float delta_time, float aspect);
	};
}
