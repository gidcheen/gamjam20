#pragma once

#include <memory>
#include <optional>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <text.hpp>

#include "object.hpp"
#include "bounding_box.hpp"

namespace graphics {
	class camera;
	class renderable;
	class renderer;
	class sprite;
}

struct GLFWwindow;

namespace game {
	using namespace glm;
	using namespace std;
	using namespace graphics;
	using namespace ui;

	class pickupable;
	class interactable;
	class world;

	class player : public object {
	private:
		shared_ptr<renderer> player_renderer;
		GLFWwindow * window;

		shared_ptr<sprite> sprite_left;
		shared_ptr<sprite> sprite_right;

		shared_ptr<renderable> player_renderable;

		shared_ptr<font> pixel_font;
		shared_ptr<text> interact_text;

		bounding_box bb;
		weak_ptr<pickupable> current_pickupable;

		int last_interact_key_state{GLFW_RELEASE};
		const float speed{80};

		const vec2 min_pos = vec2(-256, 0);
		const vec2 max_pos = vec2(256, 64);

		float get_horizontal_input() const;
		float get_vertical_input() const;
		void handle_interactions(world & w);

	public:
		player(GLFWwindow * window, shared_ptr<renderer> player_renderer, shared_ptr<camera> cam, shared_ptr<font> pixel_font, mat3 transform);
		player(const player &) = delete;
		~player() override;

		void update(world & main_world, float delta_time, float aspect) override;
		bool needs_update() override;


		bounding_box get_bounding_box() const;

		void pick_up(shared_ptr<pickupable> to_pickup);
		shared_ptr<pickupable> get_current_pickupable() const;
	};
}
