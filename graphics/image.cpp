#include "image.hpp"

#include <stb/stb_image.h>
#include <string>

namespace graphics {
	image::image(vector<pixel> pixels, size_t width, size_t height) :
		pixels(move(pixels)),
		width(width),
		height(height) {
	}

	vector<image::pixel> const & image::get_pixels() const {
		return pixels;
	}

	size_t image::get_width() const {
		return width;
	}

	size_t image::get_height() const {
		return height;
	}

	optional<image> image::load(string const & path) {
		int width{}, height{};
		int channels_in_file{};
		auto pixel_ptr = stbi_load(path.data(), &width, &height, &channels_in_file, 4);
		if (pixel_ptr) {
			auto size = size_t(width) * size_t(height) * 4;
			vector<image::pixel> pixels(size);
			memcpy(pixels.data(), pixel_ptr, size);
			stbi_image_free(pixel_ptr);
			return make_optional<image>(pixels, width, height);
		} else {
			return nullopt;
		}
	}
}
