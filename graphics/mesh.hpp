#pragma once

#include <glad/glad.h>

namespace graphics {
	class mesh {
	private:
		GLuint vertex_buffer{};
		GLuint vertex_array{};

	public:
		explicit mesh();
		mesh(mesh const &) = delete;
		mesh(mesh && other) noexcept;
		~mesh();

        [[maybe_unused]] [[nodiscard]] GLuint get_vertex_buffer() const noexcept;
        [[maybe_unused]] [[nodiscard]] GLuint get_vertex_array() const noexcept;
	};
}
