#pragma once

#include <glm/glm.hpp>

namespace graphics {
	using namespace std;
	using namespace glm;

	class camera {
	public:
		float width{1000};
		mat3 transform{mat3(1)};
	};
}
