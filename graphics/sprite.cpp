#include "sprite.hpp"

#include <array>

namespace graphics {
	sprite::sprite(graphics::image i) :
		img(move(i)) {
		glCreateTextures(GL_TEXTURE_2D, 1, &texture);
		glTextureStorage2D(texture, 1, GL_RGBA8, img.get_width(), img.get_height());
		glTextureSubImage2D(texture, 0, 0, 0, img.get_width(), img.get_height(), GL_RGBA, GL_UNSIGNED_BYTE,
                            img.get_pixels().data());
		glTextureParameteri(texture, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	sprite::sprite(sprite && other) :
		img(move(other.img)),
		texture(other.texture) {
		other.texture = 0;
	}

	sprite::~sprite() {
		if (texture) {
			glDeleteTextures(1, &texture);
		}
	}

	image const & sprite::get_image() const {
		return img;
	}

	GLuint sprite::get_texture() const {
		return texture;
	}
}
