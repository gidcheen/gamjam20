#include "mesh.hpp"

#include <array>

#include <glm/glm.hpp>

namespace graphics {
	using namespace std;
	using namespace glm;

	static const array<vec4, 4> vertices{
		vec4(0.0f, 0.0f, 0.0f, 0.0f),
		vec4(0.0f, 1.0f, 0.0f, 0.0f),
		vec4(1.0f, 1.0f, 0.0f, 0.0f),
		vec4(1.0f, 0.0f, 0.0f, 0.0f),
	};

	mesh::mesh() {
		glCreateBuffers(1, &vertex_buffer);
		glNamedBufferData(vertex_buffer, sizeof(vertices), vertices.data(), GL_STATIC_DRAW);

		glCreateVertexArrays(1, &vertex_array);
		glVertexArrayVertexBuffer(vertex_array, 0, vertex_buffer, 0, sizeof(vec4));
		glEnableVertexArrayAttrib(vertex_array, 0);
	}

	mesh::mesh(mesh && other)  noexcept :
		vertex_buffer(other.vertex_buffer),
		vertex_array(other.vertex_array) {
		other.vertex_array = 0;
		other.vertex_buffer = 0;
	}

	mesh::~mesh() {
		if (vertex_buffer && vertex_array) {
			glDeleteBuffers(1, &vertex_buffer);
			glDeleteVertexArrays(1, &vertex_array);
		}
	}

    [[maybe_unused]] GLuint mesh::get_vertex_buffer() const {
		return vertex_buffer;
	}

    [[maybe_unused]] GLuint mesh::get_vertex_array() const {
		return vertex_array;
	}
}
