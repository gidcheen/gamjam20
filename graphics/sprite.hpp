#pragma once

#include <cstdint>
#include <string>
#include <vector>
#include <array>
#include <optional>

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "image.hpp"

namespace graphics {
	using namespace std;
	using namespace glm;

	class sprite {
	private:
		image img;
		GLuint texture{};

	public:
		explicit sprite(image image);
		sprite(sprite const &) = delete;
		sprite(sprite && other);
		~sprite();

		image const & get_image() const;
		GLuint get_texture() const;
	};
}
