#pragma once

#include <vector>
#include <memory>

namespace graphics {
	class renderable;

	using namespace std;

	class renderer {
	public:
		void submit(weak_ptr<renderable> const & renderable);
		void remove(weak_ptr<renderable> const & renderable);

		void render(float aspect);
	private:
		vector<weak_ptr<renderable>> renderables;
	};
}
