#pragma once

#include <memory>
#include <map>
#include <cinttypes>

#include <glad/glad.h>
#include <glm/glm.hpp>

namespace graphics {
	using namespace std;
	using namespace glm;

	class material {
	public:
		bool is_transparent{false};
		map<GLuint, uint32_t> ints;
		map<GLuint, float> floats;
		map<GLuint, vec2> vec2s;
		map<GLuint, vec3> vec3s;
	};
}
