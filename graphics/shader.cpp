#include "shader.hpp"

#include <iostream>
#include <string>
#include <array>
#include <vector>

namespace graphics {
	using namespace std;

	char const * base_vert_src = R"glsl(
#version 450

layout (location = 0) in vec2 position;

layout(location = 0) uniform float depth;
layout(location = 1) uniform mat3 transform;

layout(location = 2) uniform mat3 camera_transform;
layout(location = 3) uniform vec2 frame_size;

layout(location = 4) uniform vec2 image_size;

layout(location = 0) out vec2 uv;

vec2 get_position();

void main() {
	uv = vec2(position.x, 1 - position.y);

	mat3 im = mat3(      image_size.x, 0, 0, 0,       image_size.y, 0, 0, 0, 1);
	mat3 fr = mat3(1.0 / frame_size.x, 0, 0, 0, 1.0 / frame_size.y, 0, 0, 0, 1);

	vec3 pos = fr * camera_transform * transform * im * vec3(get_position(), 1.0);
	pos.z = depth;
    gl_Position = vec4(pos, 1.0);
}
)glsl";

	char const * default_vert_src = R"glsl(
vec2 get_position(){ return position; }
)glsl";

	char const * base_frag_src = R"glsl(
#version 450

layout(binding = 0) uniform sampler2D sprite;

layout(location = 0) out vec4 out_color;

layout(location = 0) in vec2 uv;

vec4 get_color();

void main() {
    out_color = get_color();
	if (out_color.a <= 0) {
		discard;
	}
}
)glsl";

	char const * default_frag_src = R"glsl(
vec4 get_color(){ return texture(sprite, uv); }
)glsl";


	void validate_shader(GLuint shader) {
		GLint result;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
		if (result == GL_FALSE) {
			GLint lenght;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &lenght);
			vector<char> error(lenght);
			glGetShaderInfoLog(shader, lenght, &lenght, error.data());
			cerr << "shader error" + string(error.data()) << endl;
		}
	}

	void validate_program(GLuint program) {
		GLint result;
		glGetProgramiv(program, GL_LINK_STATUS, &result);
		if (result == GL_FALSE) {
			GLint lenght;
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &lenght);
			vector<char> error(lenght);
			glGetProgramInfoLog(program, lenght, &lenght, error.data());
			cerr << "shader error" + string(error.data()) << endl;
		}
	}

	shader::shader(optional<char const *> vert_src, optional<char const *> frag_src) :
		program(glCreateProgram()) {
		array<char const *, 2> vert_srces{base_vert_src, vert_src ? vert_src.value() : default_vert_src};
		GLuint vert = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vert, vert_srces.size(), vert_srces.data(), nullptr);
		glCompileShader(vert);
		validate_shader(vert);
		glAttachShader(program, vert);

		array<char const *, 2> frag_srces{base_frag_src, frag_src ? frag_src.value() : default_frag_src};
		GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(frag, frag_srces.size(), frag_srces.data(), nullptr);
		glCompileShader(frag);
		validate_shader(frag);
		glAttachShader(program, frag);

		glLinkProgram(program);
		validate_program(program);
		glValidateProgram(program);

		glDeleteShader(vert);
		glDeleteShader(frag);
	}

	shader::shader(shader && other) noexcept :
		program(other.program) {
		other.program = 0;
	}

	shader::~shader() {
		if (program) {
			glDeleteProgram(program);
		}
	}

	GLuint shader::get_program() {
		return program;
	}
}
