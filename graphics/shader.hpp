#pragma once

#include <glad/glad.h>
#include <optional>

namespace graphics {
	using namespace std;

	class shader {
	private:
		GLuint program{};
	public:
		explicit shader(optional<char const *> vert_src, optional<char const *> frag_src);
		shader(shader const &) = delete;
		shader(shader && other) noexcept;
		~shader();

		GLuint get_program();
	};
}
