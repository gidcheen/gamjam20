#include "renderer.hpp"

#include <algorithm>

#include "renderable.hpp"
#include "mesh.hpp"
#include "camera.hpp"
#include "shader.hpp"
#include "material.hpp"
#include "sprite.hpp"

namespace graphics {
	bool sort_renderables(weak_ptr<renderable> const & a, weak_ptr<renderable> const & b) {
		auto aa = a.lock();
		auto bb = b.lock();
		if (aa == nullptr || bb == nullptr) {
			return false;
		}
		if (aa->material_ptr->is_transparent != bb->material_ptr->is_transparent) {
			return !aa->material_ptr->is_transparent;
		}
		if (aa->material_ptr->is_transparent) {
			return aa->depth > bb->depth;
		} else {
			return aa->depth < bb->depth;
		}
	}

	void renderer::submit(weak_ptr<renderable> const & renderable) {
		renderables.emplace_back(renderable);
		sort(begin(renderables), end(renderables), sort_renderables);
	}

	void renderer::remove(weak_ptr<renderable> const & renderable) {
		auto it = find_if(begin(renderables), end(renderables), [&renderable](weak_ptr<graphics::renderable> const & r) {
			return r.lock() == renderable.lock();
		});
		if (it != end(renderables)) {
			renderables.erase(it);
		}
	}

	void renderer::render(float aspect) {
		for (auto & r : renderables) {
			auto rr = r.lock();
			if (!rr) {
				continue;
			}

			// shader
			glUseProgram(rr->shader_ptr->get_program());

			// material
			if (rr->material_ptr->is_transparent) {
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			} else {
				glDisable(GL_BLEND);
			}
			for (auto & i : rr->material_ptr->ints) {
				glUniform1i(i.first, i.second);
			}
			for (auto & i : rr->material_ptr->floats) {
				glUniform1f(i.first, i.second);
			}
			for (auto & i : rr->material_ptr->vec2s) {
				glUniform2f(i.first, i.second.x, i.second.y);
			}
			for (auto & i : rr->material_ptr->vec3s) {
				glUniform3f(i.first, i.second.x, i.second.y, i.second.z);
			}

			// mesh
			glBindVertexArray(rr->mesh_ptr->get_vertex_array());
			
			// camera
			auto inverse = glm::inverse(rr->camera_ptr->transform);
			glUniformMatrix3fv(2, 1, GL_FALSE, &inverse[0][0]);
			glUniform2f(3, rr->camera_ptr->width, rr->camera_ptr->width * aspect);

			//sprite
			glUniform2f(4, (GLfloat)rr->sprite_ptr->get_image().get_width(), (GLfloat)rr->sprite_ptr->get_image().get_height());
			glBindTextureUnit(0, rr->sprite_ptr->get_texture());

			// renderable
			glUniform1f(0, rr->depth);
			glUniformMatrix3fv(1, 1, GL_FALSE, &rr->transform[0][0]);

			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		}
	}
}
