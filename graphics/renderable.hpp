#pragma once

#include <memory>

#include <glm/glm.hpp>

namespace graphics {
	using namespace std;
	using namespace glm;

	class camera;
	class mesh;
	class shader;
	class material;
	class sprite;

	class renderable {
	public:
		shared_ptr<shader> shader_ptr;
		shared_ptr<material> material_ptr;
		shared_ptr<mesh> mesh_ptr;
		shared_ptr<camera> camera_ptr;
		shared_ptr<sprite> sprite_ptr;
		float depth;
		mat3 transform;

		explicit renderable(
			shared_ptr<shader> shader_ptr,
			shared_ptr<material> material_ptr,
			shared_ptr<mesh> mesh_ptr,
			shared_ptr<camera> camera_ptr,
			shared_ptr<sprite> sprite_ptr,
			float depth,
			mat3 transform
		) :
			shader_ptr(move(shader_ptr)),
			material_ptr(move(material_ptr)),
			mesh_ptr(move(mesh_ptr)),
			camera_ptr(move(camera_ptr)),
			sprite_ptr(move(sprite_ptr)),
			depth(depth),
			transform(transform) {}
	};
}
