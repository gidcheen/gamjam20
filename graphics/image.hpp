#pragma once

#include <vector>
#include <array>
#include <cstdint>
#include <optional>
#include <string>

namespace graphics {
	using namespace std;

	class image {
	public:
		struct pixel {
			union {
				struct {
					uint8_t r;
					uint8_t g;
					uint8_t b;
					uint8_t a;
				};
				array<uint8_t, 4> data;
			};
		};

	private:
		vector<pixel> pixels;
		size_t width;
		size_t height;

	public:
		image(vector<pixel> pixels, size_t width, size_t height);

		[[nodiscard]] vector<pixel> const & get_pixels() const;
		[[nodiscard]] size_t get_width() const;
		[[nodiscard]] size_t get_height() const;

		static optional<image> load(string const & path);
	};
}
