#pragma once

#include <functional>
#include <sprite.hpp>

#include "text.hpp"

namespace ui {
	using namespace std;
	using namespace graphics;

	class button {
	private:
		function<void()> callback;
		sprite s;
	};
}
