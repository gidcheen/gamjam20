#include "text.hpp"

#include <utility>
#include <renderable.hpp>
#include <material.hpp>

namespace ui {
	text::text(
		shared_ptr<renderer> text_renderer,
		shared_ptr<camera> text_camera,
		shared_ptr<font> text_font,
		string text_to_render,
		float depth,
		mat3 transform
	) :
		text_renderer(move(text_renderer)),
		text_font(move(text_font)),
		text_camera(move(text_camera)),
		text_to_render(""),
		depth(depth),
		transform(transform) {
		set_text(text_to_render);
	}

	text::text(text && other) :
		text_renderer(move(other.text_renderer)),
		text_font(move(other.text_font)),
		text_camera(move(other.text_camera)),
		text_to_render(move(other.text_to_render)),
		char_renderables(move(other.char_renderables)),
		depth(other.depth),
		transform(other.transform) {
		other.char_renderables.clear();
	}

	text::~text() {
		for (auto & r: char_renderables) {
			text_renderer->remove(r);
		}
	}

	void text::set_depth(float value) {
		for (auto & r : char_renderables) {
			r->depth = value;
		}
	}

	void text::set_transform(mat3 value) {
		for (auto & r : char_renderables) {
			r->transform = value;
		}
	}

	void text::set_text(string t) {
		if (text_to_render == t) {
			return;
		}
		text_to_render = move(t);

		for (auto & c : char_renderables) {
			text_renderer->remove(c);
		}
		char_renderables.clear();

		size_t i = 0;
		for (auto c : this->text_to_render) {
			auto char_material = make_shared<material>();
			char_material->ints[20] = i;
			char_material->ints[21] = c - 32;

			renderable char_renderable(
				this->text_font->char_shader,
				char_material,
				this->text_font->char_mesh,
				this->text_camera,
				this->text_font->char_sprite,
				this->depth,
				this->transform
			);
			char_renderables.emplace_back(make_shared<renderable>(char_renderable));
			i++;
		}

		for (auto & r: char_renderables) {
			text_renderer->submit(r);
		}
	}
	string text::get_text() {
		return text_to_render;
	}
}