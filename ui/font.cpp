#include "font.hpp"


namespace ui {
	char const * vert_src = R"glsl(

layout(location = 20) uniform int number;
vec2 get_position(){
	vec2 offset_position = position;
	offset_position.x = offset_position.x / 95.0;
	offset_position.x += number / 95.0;
	return offset_position;
}

)glsl";

	char const * frag_src = R"glsl(

layout(location = 21) uniform int character;
vec4 get_color(){
	vec2 offset_uv = uv;
	offset_uv.x += character;
	offset_uv.x /= 95.0;

	vec4 color = texture(sprite, offset_uv);
	return color;
}

)glsl";

	font::font(shared_ptr<sprite> char_sprite) :
		char_shader(make_shared<shader>(make_optional(vert_src), make_optional(frag_src))),
		//char_shader(make_shared<shader>(nullopt, nullopt)),
		char_mesh(make_shared<mesh>()),
		char_sprite(move(char_sprite)) {

	}
}
