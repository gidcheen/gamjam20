#pragma once

#include <memory>
#include <shader.hpp>
#include <material.hpp>
#include <mesh.hpp>
#include <camera.hpp>
#include <sprite.hpp>

namespace ui {
	using namespace std;
	using namespace graphics;

	struct font {
		const shared_ptr<shader> char_shader;
		const shared_ptr<mesh> char_mesh;
		const shared_ptr<sprite> char_sprite;
		explicit font(shared_ptr<sprite> char_sprite);
	};
}
