project(ui)

add_library(ui STATIC
        button.cpp
        font.cpp
        text.cpp
        )

target_include_directories(ui PUBLIC .)
target_link_libraries(ui graphics)