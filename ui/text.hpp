#pragma once

#include <memory>
#include <string>
#include <vector>

#include <glm/glm.hpp>
#include <renderer.hpp>

#include "font.hpp"

namespace ui {
	using namespace std;
	using namespace glm;
	using namespace graphics;

	class text {
	public:
	private:
		shared_ptr<renderer> text_renderer;
		shared_ptr<font> text_font;
		shared_ptr<camera> text_camera;

		string text_to_render;
		vector<shared_ptr<renderable>> char_renderables;
		float depth;
		mat3 transform;

	public:
		explicit text(
			shared_ptr<renderer> text_renderer,
			shared_ptr<camera> text_camera,
			shared_ptr<font> text_font,
			string text_to_render,
			float depth,
			mat3 transform
		);
		text(text const &) = delete;
		text(text && other);
		~text();

		void set_depth(float value);
		void set_transform(mat3 value);
		void set_text(string t);
		string get_text();
	};
}
